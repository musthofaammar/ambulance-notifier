package id.ateam.ambulancenotifier.data.networking.response.login

data class User(
    var email: String,
    var fullname: String,
    var role: Int
)