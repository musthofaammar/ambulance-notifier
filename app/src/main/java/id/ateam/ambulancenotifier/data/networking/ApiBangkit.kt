package id.ateam.ambulancenotifier.data.networking

import id.ateam.ambulancenotifier.data.networking.body.LoginBody
import id.ateam.ambulancenotifier.data.networking.body.RegisterBody
import id.ateam.ambulancenotifier.data.networking.response.ambulance.AmbulanceResponse
import id.ateam.ambulancenotifier.data.networking.response.login.LoginResponse
import id.ateam.ambulancenotifier.data.networking.response.register.RegisterResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiBangkit {
    @POST("user")
    suspend fun register(
        @Body body: RegisterBody
    ): Response<RegisterResponse>
    @POST("user/login")
    suspend fun login(
        @Body body: LoginBody
    ): Response<LoginResponse>
    @GET("ambulance/{id}")
    suspend fun getAmbulance(
        @Path ("id") id: Int,
    ): Response<AmbulanceResponse>
}