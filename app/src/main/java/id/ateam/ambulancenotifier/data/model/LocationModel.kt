package id.ateam.ambulancenotifier.data.model

data class LocationModel(
    val address: String,
    val details: String,
)