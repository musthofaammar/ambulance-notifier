package id.ateam.ambulancenotifier.data.networking.body

data class LoginBody(
    val email: String,
    val password: String
)