package id.ateam.ambulancenotifier.data.networking.response.ambulance

data class AmbulanceResponse(
    var id_ambulance: Int?,
    var latitude: String?,
    var license_plate: String?,
    var longitude: String?,
    var origin: String?,
    var status: Int,
    var type: String?
)