package id.ateam.ambulancenotifier.data

import android.content.Context
import androidx.core.content.edit

class UserPreference(context: Context) {
    companion object{
        private const val PREF_NAME = "userPreference"
        private const val AMBULANCE_ORIGIN = "ambulanceOrigin"
        private const val USER_ROLE = "userRole"
        private const val USER_NAME = "userName"
        private const val USER_EMAIL = "userEmail"
        private const val AMBULANCE_TYPE = "type"
        private const val AMBULANCE_NUMBER = "number"
        private const val AMBULANCE_LOCATION = "location"
        private const val AMBULANCE_STATUS = "status"
    }

    private val preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    fun setUser(name: String, email: String, role: Int){
        preferences.edit {
            putInt(USER_ROLE, role)
            putString(USER_NAME, name)
            putString(USER_EMAIL, email)
        }
    }

    fun setAmbulance(type: String, number: String, location: String, origin: String){
        preferences.edit {
            putString(AMBULANCE_TYPE, type)
            putString(AMBULANCE_NUMBER, number)
            putString(AMBULANCE_LOCATION, location)
            putString(AMBULANCE_ORIGIN, origin)
        }
    }

    fun setAmbulanceStatus(status: Int){
        preferences.edit{
            putInt(AMBULANCE_STATUS, status)
        }
    }

    fun getRole() = preferences.getInt(USER_ROLE, 101)
    fun getName() = preferences.getString(USER_NAME, "Nanditya Nuswatama")
    fun getEmail() = preferences.getString(USER_EMAIL, "gx11@gmail.com")
    fun getAmbulanceOrigin() = preferences.getString(AMBULANCE_ORIGIN, "-7.768349, 110.373634")
    fun getAmbulanceType() = preferences.getString(AMBULANCE_TYPE, "XXX")
    fun getAmbulanceNumber() = preferences.getString(AMBULANCE_NUMBER, "XXX")
    fun getAmbulanceLocation() = preferences.getString(AMBULANCE_LOCATION, "RSUP dr. Sardjito")
    fun getAmbulanceStatus() = preferences.getInt(AMBULANCE_STATUS, 0)

}