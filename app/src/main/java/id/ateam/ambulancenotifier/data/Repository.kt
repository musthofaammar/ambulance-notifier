package id.ateam.ambulancenotifier.data

import id.ateam.ambulancenotifier.data.networking.body.LoginBody
import id.ateam.ambulancenotifier.data.networking.body.RegisterBody
import id.ateam.ambulancenotifier.utils.DataMapper
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

class Repository(private val remoteDataSource: RemoteDataSource, private val userPreference: UserPreference) {
    @FlowPreview
    fun searchLocation(search: String, key: String) =
        remoteDataSource.searchLocation(search, userPreference.getAmbulanceOrigin().toString(), key)
            .debounce(300)
            .distinctUntilChanged()
            .map { DataMapper.mapSearchToModel(it) }

    fun getRoute(key: String, destination: String, origin: String) =
        remoteDataSource.getRoutes(key, destination, origin)

    fun register(body: RegisterBody) =
        remoteDataSource.register(body)
            .map {
                userPreference.setUser(
                it.result.fullname,
                it.result.email,
                it.result.role.toInt())
                it
            }

    fun login(body: LoginBody) =
        remoteDataSource.login(body)
            .map {
                userPreference.setUser(
                    it.user.fullname,
                    it.user.email,
                    it.user.role
                )
                it
                }

    fun getAmbulance(id: Int) =
        remoteDataSource.getAmbulanceDetail(id)
            .map {
                userPreference.setAmbulance(
                    it.type.toString(),
                    it.license_plate.toString(),
                    it.origin.toString(),
                    (it.latitude+","+it.longitude)
                )
                userPreference.setAmbulanceStatus(
                    it.status
                )
                it
            }
}