package id.ateam.ambulancenotifier.data.networking.response.register

data class RegisterResponse(
    var result: Result,
    var success: Int?
)