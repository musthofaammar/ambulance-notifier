package id.ateam.ambulancenotifier.data.networking.body

data class RegisterBody(
    var email: String,
    var password: String,
    var fullname: String,
    var role: Int?=101
)