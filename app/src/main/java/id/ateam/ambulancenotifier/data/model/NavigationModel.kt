package id.ateam.ambulancenotifier.data.model

data class NavigationModel (
    val index: Int,
    val distance: String,
    val time: String,
    val summary: String
)