package id.ateam.ambulancenotifier.data.networking.response

data class SearchPlaceResponse(
    val status: String,
    val predictions: ArrayList<ListPlace>
)

data class ListPlace(
    val description: String
)