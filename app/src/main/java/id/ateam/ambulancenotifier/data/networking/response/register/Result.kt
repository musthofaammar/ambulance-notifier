package id.ateam.ambulancenotifier.data.networking.response.register

data class Result(
    var email: String,
    var fullname: String,
    var id_user: Double?,
    var password: Any?,
    var role: Double
)