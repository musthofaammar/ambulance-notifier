package id.ateam.ambulancenotifier.data.networking.response.login

data class LoginResponse(
    var msg: String,
    var success: Int,
    var user: User
)