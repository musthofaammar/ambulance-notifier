package id.ateam.ambulancenotifier.data

import id.ateam.ambulancenotifier.data.networking.ApiGoogle
import id.ateam.ambulancenotifier.data.networking.ApiBangkit
import id.ateam.ambulancenotifier.data.networking.body.LoginBody
import id.ateam.ambulancenotifier.data.networking.body.RegisterBody
import id.ateam.ambulancenotifier.data.networking.response.DetailDirectionMapsResponse
import id.ateam.ambulancenotifier.data.networking.response.SearchPlaceResponse
import id.ateam.ambulancenotifier.data.networking.response.ambulance.AmbulanceResponse
import id.ateam.ambulancenotifier.data.networking.response.login.LoginResponse
import id.ateam.ambulancenotifier.data.networking.response.register.RegisterResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import timber.log.Timber

class RemoteDataSource(private val apiBangkit: ApiBangkit, private val apiGoogle: ApiGoogle) {
    fun searchLocation(search: String, location: String, key: String): Flow<SearchPlaceResponse> {
        return flow {
            val radius = 15000 // 15 KM
            try {
                val response = apiGoogle.searchPlace(search, location, radius, true, key)
                if (response.isSuccessful) {
                    val data = response.body() as SearchPlaceResponse
                    emit(data)
                } else {
                    Timber.d("Search Error")
                }
            } catch (e: Exception) {
                Timber.e(e.message.toString())
            }
        }.flowOn(Dispatchers.IO)
    }

    fun getRoutes(
        key: String,
        destination: String,
        origin: String
    ): Flow<DetailDirectionMapsResponse> {
        return flow {
            val mode = "car"
            try {
                val response = apiGoogle.getDetailDirection(mode, key, destination, true, origin)
                if (response.isSuccessful) {
                    val data = response.body() as DetailDirectionMapsResponse
                    emit(data)
                }
            } catch (e: Exception) {
                Timber.e(e.message.toString())
            }
        }.flowOn(Dispatchers.IO)
    }

    fun register(body: RegisterBody): Flow<RegisterResponse> {
        return flow {
            try {
                val response = apiBangkit.register(body)
                if (response.isSuccessful) {
                    val data = response.body() as RegisterResponse
                    emit(data)
                }
            } catch (e: Exception) {
                Timber.e(e.message.toString())
            }
        }.flowOn(Dispatchers.IO)
    }

    fun login(body: LoginBody): Flow<LoginResponse> {
        Timber.tag("RDS").d("Login ${body.email} RDS")
        return flow {
            Timber.tag("RDS").d("Login in return ${body.email} RDS")
            try {
                val response = apiBangkit.login(body)
                if (response.isSuccessful) {
                    val data = response.body() as LoginResponse
                    emit(data)
                }
                else{
                    Timber.tag("RDS").d(response.errorBody().toString())
                }
            } catch (e: Exception) {
                Timber.e(e.message.toString())
            }
        }.flowOn(Dispatchers.IO)
    }

    fun getAmbulanceDetail(id: Int): Flow<AmbulanceResponse> {
        Timber.tag("RDS").d("GetAmbulance RDS")
        return flow {
            try {
                val response = apiBangkit.getAmbulance(id)
                if (response.isSuccessful) {
                    val data = response.body() as AmbulanceResponse
                    emit(data)
                }
            } catch (e: Exception) {
                Timber.e(e.message.toString())
            }
        }.flowOn(Dispatchers.IO)
    }
}