package id.ateam.ambulancenotifier.data.networking

import id.ateam.ambulancenotifier.data.networking.response.DetailDirectionMapsResponse
import id.ateam.ambulancenotifier.data.networking.response.SearchPlaceResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiGoogle {
    //  Fungsi untuk mencari tempat di google maps
    @GET("place/autocomplete/json")
    suspend fun searchPlace(
        @Query("input") search: String,
        @Query("location") location: String,
        @Query("radius") radius: Int,
        @Query("strictbounds") strict: Boolean,
        @Query("key") apiKey: String
    ): Response<SearchPlaceResponse>

    @GET("directions/json")
    suspend fun getDetailDirection(
        @Query("mode") mode: String,
        @Query("key") apiKey: String,
        @Query("destination") destination: String,
        @Query("alternatives") alternatives: Boolean,
        @Query("origin") origin: String
    ): Response<DetailDirectionMapsResponse>
}