package id.ateam.ambulancenotifier

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.socket.client.IO
import io.socket.client.Socket
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val mSocket: Socket by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        connectSocket()
    }

    private fun connectSocket() {
        mSocket.connect()
        val options = IO.Options()
        options.reconnection = true
        options.forceNew = true
    }
}