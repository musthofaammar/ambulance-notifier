package id.ateam.ambulancenotifier.utils

class Constant {
    companion object{
        const val DRIVER = 110
        const val USER = 101
        const val REQUEST_GPS = 301
    }
}