package id.ateam.ambulancenotifier.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.work.*
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import io.socket.client.Socket
import org.json.JSONObject
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class AmbulanceLocationSenderWorker(private val context: Context, workerParams: WorkerParameters) :
    Worker(
        context,
        workerParams
    ), KoinComponent {

    private val mSocket: Socket by inject()
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var workManager: WorkManager? = null

    override fun doWork(): Result {

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        workManager = WorkManager.getInstance(context)

        if (!mSocket.connected()) {
            mSocket.connect()
        }

        val destination =
            LatLng(inputData.getDouble("latitude", 0.0), inputData.getDouble("longitude", 0.0))

        actualWork(destination, inputData.getString("ambulance_id"))
        return Result.success()
    }

    private fun actualWork(destination: LatLng, ambulanceId: String?) {
        try {
            Thread.sleep(5000)
            getLastLocation(destination, ambulanceId)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getLastLocation(destination: LatLng, ambulanceId: String?) {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            if (location != null) {

                val range = Helper.haversine(
                    LatLng(
                        location.latitude,
                        location.longitude
                    ), destination
                )

                println(
                    "Different range location $range"
                )

                if (range < 0.2) {
                    workManager?.cancelAllWork()
                    println("Ambulance Sampai")
                    return@addOnSuccessListener
                } else {

                    mSocket.emit("ambulance_location", JSONObject().apply {
                        put("ambulance_id", ambulanceId)
                        put("latitude", location.latitude)
                        put("longitude", location.longitude)
                    })

                    sendWorker()
                }
            } else {
                requestLocation(destination, ambulanceId)
            }
        }
    }

    private fun sendWorker() {
        val senderWorker = OneTimeWorkRequestBuilder<AmbulanceLocationSenderWorker>()
            .setConstraints(
                Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED)
                    .build()
            )
            .setConstraints(
                Constraints.Builder().setRequiresBatteryNotLow(true).build()
            )
            .addTag("senderService")
            .build()

        workManager?.enqueueUniqueWork(
            "Periodic Socket Service",
            ExistingWorkPolicy.REPLACE,
            senderWorker
        )
    }

    private fun requestLocation(destination: LatLng, ambulanceId: String?) {
        val mLocationRequest = LocationRequest.create()
        mLocationRequest.interval = 20000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult) {
                if (result != null) {
                    val location = result.locations[0]


                    val range = Helper.haversine(
                        LatLng(
                            location.latitude,
                            location.longitude
                        ), destination
                    )

                    println(
                        "Different range location $range"
                    )

                    if (range < 0.2) {
                        workManager?.cancelAllWork()
                        return
                    } else {
                        mSocket.emit("ambulance_location", JSONObject().apply {
                            put("ambulance_id", ambulanceId)
                            put("latitude", location.latitude)
                            put("longitude", location.longitude)
                        })

                        sendWorker()
                    }
                }
            }
        }

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        fusedLocationClient?.requestLocationUpdates(mLocationRequest, mLocationCallback, null)
    }
}