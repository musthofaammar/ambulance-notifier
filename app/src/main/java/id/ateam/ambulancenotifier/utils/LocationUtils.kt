package id.ateam.ambulancenotifier.utils

import android.Manifest
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.os.Build
import com.google.android.gms.maps.model.LatLng
import pub.devrel.easypermissions.EasyPermissions
import java.util.*

object LocationUtils {
    fun hasLocationPermission(context: Context) =
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            EasyPermissions.hasPermissions(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CAMERA
            )
        } else {
            EasyPermissions.hasPermissions(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CAMERA
            )
        }

    fun getMainAddress(string: String) = string.substringBefore(",", string)

    fun getLastAddress(string: String) = string.substringAfter(", ")

    fun decodeAddress(context: Context, latLng: LatLng): String {
        val geoCoder = Geocoder(context, Locale.getDefault())
        val addresses: List<Address> = geoCoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
        return if (addresses.isNullOrEmpty()) ""
        else {
            val address = addresses[0]
            with(address){
                (0..maxAddressLineIndex).joinToString(separator = "\n") { getAddressLine(it) }
            }
        }
    }

    fun getLongLat(context: Context, place: String): LatLng{
        val geoCoder = Geocoder(context, Locale.getDefault())
        val addresses = geoCoder.getFromLocationName(place, 3)
        val address = addresses[0]
        return LatLng(address.latitude, address.longitude)
    }
}