package id.ateam.ambulancenotifier.utils

import com.google.android.gms.maps.model.LatLng
import kotlin.math.*

class Helper {

    companion object {

        private const val earthRadiusKm: Double = 6372.8

        fun haversine(myLocation: LatLng, ambulanceLocation: LatLng): Double {
            val dLat = Math.toRadians(myLocation.latitude - ambulanceLocation.latitude);
            val dLon = Math.toRadians(myLocation.longitude - ambulanceLocation.longitude);
            val originLat = Math.toRadians(ambulanceLocation.latitude);
            val destinationLat = Math.toRadians(myLocation.latitude);

            val a =
                sin(dLat / 2).pow(2.toDouble()) + sin(dLon / 2).pow(2.toDouble()) * cos(originLat) * cos(
                    destinationLat
                );
            val c = 2 * asin(sqrt(a));
            return earthRadiusKm * c;
        }

    }

}