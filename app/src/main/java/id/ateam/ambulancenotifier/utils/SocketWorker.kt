package id.ateam.ambulancenotifier.utils

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.Looper
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.ui.ambulance.ShowAlertActivity
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONObject
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject


@KoinApiExtension
class SocketWorker(private val context: Context, workerParameters: WorkerParameters) :
    Worker(context, workerParameters), KoinComponent {

    private val mSocket: Socket by inject()
    private var fusedLocationClient: FusedLocationProviderClient? = null


    override fun doWork(): Result {

        connect()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

        if (mSocket.connected()) {

            setSocketListener()

            return Result.success()
        } else {
            mSocket.connect()
        }

        mSocket.on(Socket.EVENT_DISCONNECT) {
            mSocket.connect()
        }

        mSocket.on(Socket.EVENT_CONNECT_ERROR) {
            println("error : $it")
        }

        mSocket.on(Socket.EVENT_CONNECT) {
            setSocketListener()
        }

        return Result.success()
    }

    private fun setSocketListener() {
        mSocket.off("broadcast_ambulance_location")
        mSocket.on("broadcast_ambulance_location") { args ->
            if (args != null) {
                try {
                    val data = args[0] as JSONObject
                    val ambulanceId = data.get("ambulance_id") as String
                    val latLng = LatLng(
                        data.get("latitude") as Double,
                        data.get("longitude") as Double
                    )

                    println("Data from socket -> $args")
                    println("Data from socket change to String -> $data")

                    getLastLocation(latLng, ambulanceId)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun connect() {
        mSocket.connect()
        val options = IO.Options()
        options.reconnection = true //reconnection
        options.forceNew = true
    }

    private fun getLastLocation(latLng: LatLng, ambulanceId: String) {

        var ambulanceNow = ""

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            if (location != null) {

                val range = Helper.haversine(
                    LatLng(
                        location.latitude,
                        location.longitude
                    ), latLng
                )

                println(
                    "Different range location $range"
                )

                if (range < 0.4) {

                    if (ambulanceNow != ambulanceId) {
                        Handler(Looper.getMainLooper()).post {
                            val intent = Intent(context, ShowAlertActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            context.startActivity(intent)
                        }

                        showNotification()
                        ambulanceNow = ambulanceId
                    }
                }
            } else {
                requestLocation(latLng, ambulanceId)
            }
        }
    }

    private fun showNotification() {

        val channelID = "300"
        val notificationId = 301

        val soundUri: Uri = Uri.parse(
            "android.resource://" +
                    applicationContext.packageName +
                    "/" + R.raw.sirene_ambulance

        )

        val notificationManager: NotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Ambulance Notification"
            val descriptionText = "Ambulan dalam jarak 400 meter dari anda"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(channelID, name, importance).apply {
                description = descriptionText
            }

            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()

            channel.enableVibration(true)
            channel.setSound(soundUri, attributes)

            // Register the channel with the system
            notificationManager.createNotificationChannel(channel)
        }

        val builder = NotificationCompat.Builder(context, channelID)
            .setSmallIcon(R.drawable.ic_baseline_bus_alert_24)
            .setContentTitle("Ambulance Notification")
            .setContentText("Ambulan dalam jarak 400 meter dari anda")
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .setSound(soundUri)

        notificationManager.notify(notificationId, builder.build())
    }

    private fun requestLocation(latLng: LatLng, ambulanceId: String) {
        val mLocationRequest = LocationRequest.create()
        var ambulanceNow = ""

        mLocationRequest.interval = 20000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult) {
                if (result != null) {
                    val location = result.locations[0]
                    val range = Helper.haversine(
                        LatLng(
                            location.latitude,
                            location.longitude
                        ), latLng
                    )

                    println(
                        "Different range location $range"
                    )

                    if (range < 0.4) {
                        if (ambulanceNow != ambulanceId) {
                            Handler(Looper.getMainLooper()).post {
                                val intent = Intent(context, ShowAlertActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                context.startActivity(intent)
                            }

                            showNotification()
                            ambulanceNow = ambulanceId
                        }
                    }

                }
            }
        }

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        fusedLocationClient?.requestLocationUpdates(mLocationRequest, mLocationCallback, null)
    }
}