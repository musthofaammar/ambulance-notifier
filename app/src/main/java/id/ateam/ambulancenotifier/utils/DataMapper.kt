package id.ateam.ambulancenotifier.utils

import id.ateam.ambulancenotifier.data.model.NavigationModel
import id.ateam.ambulancenotifier.data.networking.response.DetailDirectionMapsResponse
import id.ateam.ambulancenotifier.data.networking.response.ListPlace
import id.ateam.ambulancenotifier.data.networking.response.SearchPlaceResponse

object DataMapper {
    fun mapSearchToModel(input: SearchPlaceResponse): ArrayList<ListPlace>{
        val data = ArrayList<ListPlace>()
        data.addAll(input.predictions)
        return data
    }

    fun mapRouteToModel(input: DetailDirectionMapsResponse): ArrayList<NavigationModel>{
        val data = ArrayList<NavigationModel>()
        val it = input.routes
        for (i in input.routes.indices){
            val time = it[i].legs?.first()?.duration?.text.toString()
            val distance = it[i].legs?.first()?.distance?.text.toString()
            val summary = it[i].summary.toString()
            data.add(NavigationModel(i, distance, time, summary))
        }
        return data
    }
}