package id.ateam.ambulancenotifier.di

import id.ateam.ambulancenotifier.data.RemoteDataSource
import id.ateam.ambulancenotifier.data.Repository
import id.ateam.ambulancenotifier.data.UserPreference
import id.ateam.ambulancenotifier.data.networking.ApiBangkit
import id.ateam.ambulancenotifier.data.networking.ApiGoogle
import id.ateam.ambulancenotifier.ui.ambulance.AmbulanceViewModel
import id.ateam.ambulancenotifier.ui.home.HomeViewModel
import id.ateam.ambulancenotifier.ui.login.LoginViewModel
import id.ateam.ambulancenotifier.ui.map.MapViewModel
import id.ateam.ambulancenotifier.ui.map.NavigationAdapter
import id.ateam.ambulancenotifier.ui.search.SearchLocAdapter
import id.ateam.ambulancenotifier.ui.search.SearchViewModel
import io.socket.client.IO
import io.socket.client.Socket
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URISyntaxException
import java.util.concurrent.TimeUnit

val networkModule = module {
    single {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .build()
    }
    single {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://34.101.69.158/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(get())
            .build()
        retrofit.create(ApiBangkit::class.java)
    }
    single {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://maps.googleapis.com/maps/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(get())
            .build()
        retrofit.create(ApiGoogle::class.java)
    }
}

val viewModelModule = module {
    viewModel { LoginViewModel(get(), get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { AmbulanceViewModel(get(), get()) }
    viewModel { SearchViewModel(get()) }
    viewModel { MapViewModel(get(), get()) }
}

val repositoryModule = module {
    single { Repository(get(), get()) }
}

val dataSourceModule = module {
    single { RemoteDataSource(get(), get()) }
    single { UserPreference(androidContext()) }
}

val socketModule = module {

    fun provideSocket(): Socket {

        val server = "http://34.101.69.158"

        try {
            return IO.socket(server)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
    }

    single { provideSocket() }
}

val adapterModule = module {
    factory { SearchLocAdapter() }
    factory { NavigationAdapter() }
}
