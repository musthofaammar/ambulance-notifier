package id.ateam.ambulancenotifier.ui.search

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isGone
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.data.model.LocationModel
import id.ateam.ambulancenotifier.databinding.FragmentSearchLocationBinding
import id.ateam.ambulancenotifier.shared.BaseFragment
import id.ateam.ambulancenotifier.ui.map.MapFragment.Companion.LOCATION
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

@FlowPreview
class SearchLocationFragment : BaseFragment() {
    private val searchViewModel by viewModel<SearchViewModel>()
    private val locationAdapter by inject<SearchLocAdapter>()
    private var _binding: FragmentSearchLocationBinding?=null
    override val binding: FragmentSearchLocationBinding
        get() = _binding!!
    override val viewModel: ViewModel
        get() = TODO("Not yet implemented")

    override fun observeLiveData() {
        null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSearchLocationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fabNext.isGone = true
        binding.fabNext.setOnClickListener {
            findNavController().navigate(R.id.action_searchLocationFragment_to_mapFragment, bundleOf(
                LOCATION to binding.inputLocation.text.toString()
            ))
        }
        searchLocation()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun searchLocation(){
        val key = resources.getString(R.string.google_maps_key)
        binding.inputLocation.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                lifecycleScope.launch {
                    if (s.toString().length > 20){
                        binding.fabNext.isGone = false
                    }
                    val placeName = arrayListOf<LocationModel>()

                    searchViewModel.searchLocation(s.toString(), key).observe(
                        viewLifecycleOwner,
                        { place ->
                            place.map {
                                placeName.add(
                                    LocationModel(
                                        it.description.substringBefore(","),
                                        it.description.substringAfter(", ")
                                    )
                                )
                            }
                            with(binding.rvLocation){
                                layoutManager = LinearLayoutManager(context)
                                adapter = locationAdapter
                                locationAdapter.setData(placeName)
                                locationAdapter.onItemClick = {
                                    val location = it.address+", "+it.details
                                    binding.inputLocation.setText(location)
                                }
                            }
                        })
                }
            }
        })
    }
}