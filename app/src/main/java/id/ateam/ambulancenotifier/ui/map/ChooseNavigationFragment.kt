package id.ateam.ambulancenotifier.ui.map

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.view.isGone
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.data.model.NavigationModel
import id.ateam.ambulancenotifier.databinding.FragmentChooseNavigationBinding
import id.ateam.ambulancenotifier.shared.BaseFragment
import id.ateam.ambulancenotifier.ui.map.BackNavigationFragment.Companion.BACK_ORIGIN
import id.ateam.ambulancenotifier.utils.AmbulanceLocationSenderWorker
import id.ateam.ambulancenotifier.utils.DataMapper
import id.ateam.ambulancenotifier.utils.LocationUtils
import io.socket.client.Socket
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class ChooseNavigationFragment : BaseFragment() {
    private val mViewModel by sharedViewModel<MapViewModel>()
    private val mAdapter by inject<NavigationAdapter>()
    private var _binding: FragmentChooseNavigationBinding? = null
    override val binding: FragmentChooseNavigationBinding
        get() = _binding!!
    override val viewModel: ViewModel
        get() = TODO("Not yet implemented")
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationManager: LocationManager
    private val mDestination by lazy {
        arguments?.getString(DESTINATION)
    }

    private lateinit var workManager: WorkManager
    private val mSocket: Socket by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChooseNavigationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMaps()
        with(binding.rvChooseNavigation) {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
        binding.chooseNavToolbar.toolBar.title = "Menuju Pasien"
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        locationManager =
            requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        workManager = WorkManager.getInstance(requireContext())
    }

    @SuppressLint("MissingPermission")
    private val callback = OnMapReadyCallback { googleMap ->
        map = googleMap
        map.isMyLocationEnabled = true
        map.addMarker(
            MarkerOptions().position(
                LocationUtils.getLongLat(
                    requireContext(),
                    mDestination.toString()
                )
            )
        )
        mViewModel.observeOrigin.observe(viewLifecycleOwner, {
            val latLng = LocationUtils.getLongLat(requireContext(), it)
            moveCamera(latLng)
            map.addMarker(
                MarkerOptions().position(latLng)
                    .icon(bitmapDescriptorFromVector(R.drawable.icon_ambulance))
            )
        })
        mViewModel.getRoute(resources.getString(R.string.google_maps_key), mDestination.toString())
            .observe(viewLifecycleOwner, { response ->
                val data = DataMapper.mapRouteToModel(response)
                mAdapter.setData(data)
                mAdapter.setOnItemClickCallback(object : NavigationAdapter.OnItemClickCallback {
                    override fun onItemClicked(item: NavigationModel) {
                        val routes = response.routes
                        mViewModel.updateRoute(
                            decodePolyline(routes[item.index].overviewPolyline.points.toString()),
                            item.index
                        )
                        restartFragment()
                    }
                })
            })
        val mPoly = ArrayList<LatLng>()
        val polylineOptions = PolylineOptions()
        val listColor = listOf(
            Color.parseColor("#00BCD4"),
            Color.parseColor("#60FF4F"),
            Color.parseColor("#FFEB3B")
        )
        mViewModel.observeRoute.observe(viewLifecycleOwner, { list ->
            mPoly.addAll(list)
            polylineOptions.apply {
                mViewModel.observeIndex.observe(viewLifecycleOwner, {
                    addAll(mPoly)
                    color(listColor[it])
                    width(20f)
                    map.addPolyline(this)
                })
            }
            if (list != null) {
                binding.fabNext.isGone = false
                binding.rvChooseNavigation.setPadding(0, 0, 0, 50)
                binding.fabNext.setOnClickListener {
                    val lat = getLatLong(mDestination.toString()).latitude.toString()
                    val long = getLatLong(mDestination.toString()).longitude.toString()
                    val latLng = "$lat, $long"
                    launchMap(
                        latLng,
                        LatLng(
                            getLatLong(mDestination.toString()).latitude,
                            getLatLong(mDestination.toString()).longitude
                        )
                    )
                    showBackButton()
                }
            }
        })
        map.isTrafficEnabled = true
        map.setOnCameraIdleListener {}
        map.setOnMyLocationClickListener {
            val destination = Location("")
            destination.latitude = getLatLong(mDestination.toString()).latitude
            destination.longitude = getLatLong(mDestination.toString()).longitude
            Toast.makeText(
                requireContext(),
                it.distanceTo(destination).toString(),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun setupMaps() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private fun moveCamera(latLng: LatLng) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13F))
    }

    private fun decodePolyline(poly: String): List<LatLng> {
        return PolyUtil.decode(poly)
    }

    private fun bitmapDescriptorFromVector(source: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(requireContext(), source)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap =
                Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

    private fun restartFragment() {
        mViewModel.observeOrigin.observe(viewLifecycleOwner, {
            findNavController().navigate(
                R.id.action_chooseNavigationFragment_self,
                bundleOf(DESTINATION to mDestination, BACK_ORIGIN to it)
            )
        })
    }

    private fun launchMap(end: String, destination: LatLng) {
        val gmmIntentUri = Uri.parse("google.navigation:q=$end")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")

        runAsSender(destination)

        startActivity(mapIntent)
    }

    private fun runAsSender(destination: LatLng? = null) {

        destination?.let {
            workManager.cancelAllWorkByTag("listenerService")

            mSocket.off("broadcast_ambulance_location")
            val senderWorker = OneTimeWorkRequestBuilder<AmbulanceLocationSenderWorker>()
                .setConstraints(
                    Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
                )
                .setConstraints(Constraints.Builder().setRequiresBatteryNotLow(true).build())
                .setInputData(
                    workDataOf(
                        "ambulance_id" to "AB191823",
                        "latitude" to destination.latitude,
                        "longitude" to destination.longitude
                    )
                )
                .addTag("senderService")
                .build()

            workManager.enqueueUniqueWork(
                "Periodic Socket Service",
                ExistingWorkPolicy.REPLACE,
                senderWorker
            )
        }
    }

    private fun getLatLong(place: String): LatLng {
        return LocationUtils.getLongLat(requireContext(), place)
    }

    private fun showBackButton() {
        with(binding) {
            fabNext.isGone = true
            fabBack.isGone = false
            mViewModel.observeFullLocation.observe(viewLifecycleOwner, { location ->
                fabBack.setOnClickListener {
                    findNavController().navigate(
                        R.id.action_chooseNavigationFragment_to_navigationFragment,
                        bundleOf(BACK_ORIGIN to location)
                    )
                }
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun observeLiveData() {
        null
    }

    companion object {
        const val DESTINATION = "ambulance destination"
    }
}