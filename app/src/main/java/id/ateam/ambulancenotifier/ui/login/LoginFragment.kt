package id.ateam.ambulancenotifier.ui.login

import android.Manifest
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.data.networking.body.LoginBody
import id.ateam.ambulancenotifier.databinding.FragmentLoginBinding
import id.ateam.ambulancenotifier.shared.BaseFragment
import id.ateam.ambulancenotifier.utils.LocationUtils
import kotlinx.coroutines.FlowPreview
import org.koin.android.viewmodel.ext.android.viewModel
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions

@FlowPreview
class LoginFragment : BaseFragment(), EasyPermissions.PermissionCallbacks {
    private var _binding: FragmentLoginBinding?= null
    override val binding: FragmentLoginBinding
        get() = _binding!!
    override val viewModel: ViewModel
        get() = TODO("Not yet implemented")
    private val mViewModel by viewModel<LoginViewModel>()

    override fun observeLiveData() {
        null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnLogin.setOnClickListener {
            login()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) AppSettingsDialog.Builder(this).build().show()
        else requestPermissions()
    }

    private fun requestPermissions(){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) EasyPermissions.requestPermissions(
            this,
            "Anda harus mengizinkan penggunaan lokasi untuk menggunakan aplikasi ini.",
            101,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA
        ) else EasyPermissions.requestPermissions(
            this,
            "Anda harus mengizinkan penggunaan lokasi untuk menggunakan aplikasi ini.",
            101,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
            Manifest.permission.CAMERA
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    private fun login(){
        with(binding){
           val body = LoginBody(
               inputEmail.text.toString(), inputPassword.text.toString()
           )
            mViewModel.login(body).observe(viewLifecycleOwner, {
                if (it.success == 1){
                    if (LocationUtils.hasLocationPermission(requireContext())){
                        findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                    } else {
                        requestPermissions()
                    }
                }else Toast.makeText(requireContext(), it.msg, Toast.LENGTH_SHORT).show()

            })
        }
    }
}