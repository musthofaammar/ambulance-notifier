package id.ateam.ambulancenotifier.ui.map

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.databinding.FragmentMapBinding
import id.ateam.ambulancenotifier.shared.BaseFragment
import id.ateam.ambulancenotifier.ui.map.ChooseNavigationFragment.Companion.DESTINATION
import id.ateam.ambulancenotifier.utils.Constant.Companion.REQUEST_GPS
import id.ateam.ambulancenotifier.utils.LocationUtils
import org.koin.android.viewmodel.ext.android.sharedViewModel

class MapFragment : BaseFragment() {
    private var _binding: FragmentMapBinding?=null
    override val binding: FragmentMapBinding
        get() = _binding!!
    override val viewModel: ViewModel
        get() = TODO("Not yet implemented")
    private val mViewModel by sharedViewModel<MapViewModel>()
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationManager: LocationManager
    private val mLocation by lazy {
        arguments?.getString(LOCATION)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            saveLocationBtn.setOnClickListener {
                findNavController().navigate(
                    R.id.action_mapFragment_to_chooseNavigationFragment,
                    bundleOf(DESTINATION to (locationAddress.text.toString()+", "+locationAddressDetails.text.toString())))
            }
        }

        mViewModel.updateLocation(mLocation.toString())
        setView()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        locationManager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) buildAlertMessage()
        else {
            setupMaps()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_GPS) checkLocation()
        super.onActivityResult(requestCode, resultCode, data)
    }

    @SuppressLint("MissingPermission")
    private val callback = OnMapReadyCallback{ googleMap ->
        map = googleMap
        map.isMyLocationEnabled = true
        mViewModel.observeFullLocation.observe(viewLifecycleOwner, {
            val latLng = LocationUtils.getLongLat(requireContext(), it)
            moveCamera(latLng)
        })
        map.setOnCameraIdleListener {
            mViewModel.updateLocation(LocationUtils.decodeAddress(requireContext(), map.cameraPosition.target))
        }
        map.uiSettings.isZoomGesturesEnabled = false
    }

    private fun setupMaps() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private fun moveCamera(latLng: LatLng) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15F))
    }

    private fun checkLocation(){
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            findNavController().navigateUp()
        } else {
            setupMaps()
        }
    }

    private fun setView(){
        with(mViewModel){
            binding.apply {
                observeChosenLocation.observe(viewLifecycleOwner, {
                    locationAddress.text = it
                })
                observeChosenLocationDescription.observe(viewLifecycleOwner,{
                    locationAddressDetails.text = it
                })
            }
        }
    }

    private fun buildAlertMessage() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setMessage("GPS needed, please turn on GPS")
        builder.setPositiveButton("Yes") { _, _ ->
            startActivityForResult(
                Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                REQUEST_GPS
            )
        }
        builder.setNegativeButton("No") { dialog, _ ->
            dialog.cancel()
            findNavController().navigateUp()
        }
        builder.setCancelable(false)
        builder.show()
    }
    override fun observeLiveData() {
        null
    }

    companion object{
        const val LOCATION = "destination_location"
    }
}