package id.ateam.ambulancenotifier.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.ateam.ambulancenotifier.data.UserPreference

class HomeViewModel(private val userPreference: UserPreference): ViewModel() {
    private var name = MutableLiveData<String>()
    private var email = MutableLiveData<String>()
    private var role = MutableLiveData<Int>()
    val observeName: LiveData<String> = name
    val observeEmail: LiveData<String> = email
    val observeRole: LiveData<Int> = role

    fun setUser(){
        name.postValue(userPreference.getName())
        email.postValue(userPreference.getEmail())
        role.postValue(userPreference.getRole())
    }
}