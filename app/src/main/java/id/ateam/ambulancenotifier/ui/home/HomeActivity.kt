package id.ateam.ambulancenotifier.ui.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.ateam.ambulancenotifier.databinding.ActivityHomeBinding

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}