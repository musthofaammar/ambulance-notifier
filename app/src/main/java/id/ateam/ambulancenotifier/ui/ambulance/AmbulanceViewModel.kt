package id.ateam.ambulancenotifier.ui.ambulance

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import id.ateam.ambulancenotifier.data.RemoteDataSource
import id.ateam.ambulancenotifier.data.Repository
import id.ateam.ambulancenotifier.data.UserPreference
import kotlinx.coroutines.FlowPreview

class AmbulanceViewModel(private val userPreference: UserPreference, private val repository: Repository): ViewModel() {
    private var type = MutableLiveData<String>()
    private var number = MutableLiveData<String>()
    private var origin = MutableLiveData<String>()
    private var status = MutableLiveData<Int>()
    val observeType: LiveData<String> = type
    val observeNumber: LiveData<String> = number
    val observeOrigin: LiveData<String> = origin
    val observeStatus: LiveData<Int> = status

    fun getAmbulance(id: Int) = repository.getAmbulance(id).asLiveData()

    fun setAmbulance(){
        type.postValue(userPreference.getAmbulanceType())
        number.postValue(userPreference.getAmbulanceNumber())
        origin.postValue(userPreference.getAmbulanceLocation())
        status.postValue(userPreference.getAmbulanceStatus())
    }

    fun setAmbulanceStatus(status: Int){
        userPreference.setAmbulanceStatus(status)
    }
}