package id.ateam.ambulancenotifier.ui.ambulance

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.databinding.FragmentAmbulanceDetailsBinding
import id.ateam.ambulancenotifier.shared.BaseFragment
import org.koin.android.viewmodel.ext.android.viewModel

class AmbulanceDetailsFragment : BaseFragment() {
    private var _binding: FragmentAmbulanceDetailsBinding?= null
    override val binding: FragmentAmbulanceDetailsBinding
        get() = _binding!!
    override val viewModel: ViewModel
        get() = TODO("Not yet implemented")
    private val mViewModel by viewModel<AmbulanceViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAmbulanceDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
        mViewModel.setAmbulance()
        binding.btnNext.setOnClickListener {
            findNavController().navigate(R.id.action_ambulanceDetailsFragment_to_searchLocationFragment)
            mViewModel.setAmbulanceStatus(1)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setView(){
        with(mViewModel){
            binding.apply {
                observeType.observe(viewLifecycleOwner, {tvCarName.text = it })
                observeNumber.observe(viewLifecycleOwner, {tvNumber.text = it })
                observeOrigin.observe(viewLifecycleOwner, {tvOrigin.text = it})
                observeStatus.observe(viewLifecycleOwner, {
                    if (it == 1){
                        tvStatus.text = resources.getString(R.string.active)
                    }else{
                        tvStatus.text = resources.getString(R.string.non_active)
                    }
                })
                Glide.with(requireContext())
                    .load("https://lh3.ggpht.com/p/AF1QipNNYAPLS0j6ntLLTwM2xpNAuB9n4lXfrj6VUiP7=s1536")
                    .into(imgAmbulance)
            }
        }
    }

    override fun observeLiveData() {
        null
    }
}