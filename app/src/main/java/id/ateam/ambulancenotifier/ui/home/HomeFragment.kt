package id.ateam.ambulancenotifier.ui.home

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.work.*
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.databinding.FragmentHomeBinding
import id.ateam.ambulancenotifier.ui.ambulance.AmbulanceLocationSender
import id.ateam.ambulancenotifier.utils.Constant.Companion.USER
import id.ateam.ambulancenotifier.utils.SocketWorker
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension
import pub.devrel.easypermissions.EasyPermissions
import java.util.concurrent.TimeUnit


@KoinApiExtension
class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val mViewModel by viewModel<HomeViewModel>()
    private lateinit var workManager: WorkManager

    private val permissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        workManager = WorkManager.getInstance(requireContext())
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.setUser()
        askPermission()
        setView()
        binding.scanAmbulance.imgAction.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_scanQRFragment)
        }
    }

    private fun askPermission() {
        if (EasyPermissions.hasPermissions(requireActivity(), *permissions)) {

            if (EasyPermissions.hasPermissions(
                    requireActivity(),
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
                )
            ) {
                runAsListener()

            } else {
                runAsListener()
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                    EasyPermissions.requestPermissions(
                        this,
                        "Lokasi dibutuhkan untuk mengetahui jarak ambulan dengan anda",
                        AmbulanceLocationSender.FOREGROUND_lOCATION_REQUEST_CODE,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    )
                }
            }

        } else {
            EasyPermissions.requestPermissions(
                this,
                "Lokasi dibutuhkan untuk mengetahui jarak ambulan dengan anda",
                AmbulanceLocationSender.FOREGROUND_lOCATION_REQUEST_CODE,
                *permissions
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == AmbulanceLocationSender.FOREGROUND_lOCATION_REQUEST_CODE) {
            EasyPermissions.onRequestPermissionsResult(
                AmbulanceLocationSender.FOREGROUND_lOCATION_REQUEST_CODE,
                permissions,
                grantResults,
                this
            )
        } else {
            EasyPermissions.onRequestPermissionsResult(
                AmbulanceLocationSender.BACKGROUND_lOCATION_REQUEST_CODE,
                permissions,
                grantResults,
                this
            )
        }
    }

    private fun runAsListener() {

        workManager.cancelAllWorkByTag("senderService")

        val listenerSocketWorker = PeriodicWorkRequestBuilder<SocketWorker>(
            3, TimeUnit.MINUTES
        )
            .setConstraints(
                Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
            )
            .setConstraints(Constraints.Builder().setRequiresBatteryNotLow(true).build())
            .addTag("listenerService")
            .build()

        workManager.enqueueUniquePeriodicWork(
            "Periodic Socket Service",
            ExistingPeriodicWorkPolicy.REPLACE,
            listenerSocketWorker
        )
    }

    private fun setView() {
        with(binding) {
            profile.imgIcon.setImageResource(R.drawable.icon_person)
            profile.tvAction.text = resources.getString(R.string.profile)
            bookmark.imgIcon.setImageResource(R.drawable.icon_bookmark)
            bookmark.tvAction.text = resources.getString(R.string.bookmark)
            setting.imgIcon.setImageResource(R.drawable.icon_setting)
            setting.tvAction.text = resources.getString(R.string.setting)
            about.imgIcon.setImageResource(R.drawable.icon_info)
            about.tvAction.text = resources.getString(R.string.about)
            tnc.imgIcon.setImageResource(R.drawable.icon_flag)
            tnc.tvAction.text = resources.getString(R.string.tnc)
            scanAmbulance.imgIcon.setImageResource(R.drawable.icon_scan_qr)
            scanAmbulance.tvAction.text = resources.getString(R.string.scan_kode_qr_ambulan)
            isDriver()

            with(mViewModel) {
                observeName.observe(viewLifecycleOwner, {
                    tvNameHome.text = it
                })
                observeEmail.observe(viewLifecycleOwner, {
                    tvEmail.text = it
                })
            }
        }
    }

    private fun isDriver() {
        mViewModel.observeRole.observe(viewLifecycleOwner, {
            if (it == USER) {
                with(binding.scanAmbulance) {
                    imgIcon.isGone = true
                    tvAction.isGone = true
                    imgAction.isGone = true
                }
            }
        })
    }

}