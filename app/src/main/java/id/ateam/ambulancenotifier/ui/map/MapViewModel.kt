package id.ateam.ambulancenotifier.ui.map

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.google.android.gms.maps.model.LatLng
import id.ateam.ambulancenotifier.data.Repository
import id.ateam.ambulancenotifier.data.UserPreference
import id.ateam.ambulancenotifier.utils.LocationUtils

class MapViewModel(private val repository: Repository, private val preference: UserPreference) :
    ViewModel() {
    private var fullLocation = MutableLiveData<String>()
    private var chosenLocation = MutableLiveData<String>()
    private var chosenLocationDescription = MutableLiveData<String>()
    private var origin = MutableLiveData<String>()
    private var route = MutableLiveData<List<LatLng>>()
    private var index = MutableLiveData<Int>()
    val observeFullLocation: LiveData<String> get() = fullLocation
    val observeChosenLocation: LiveData<String> get() = chosenLocation
    val observeChosenLocationDescription: LiveData<String> get() = chosenLocationDescription
    val observeOrigin: LiveData<String> get() = origin
    val observeRoute: LiveData<List<LatLng>> get() = route
    val observeIndex: LiveData<Int> get() = index

    private fun getOriginLocation() {
        origin.postValue(preference.getAmbulanceLocation())
    }

    fun updateLocation(location: String) {
        chosenLocation.postValue(LocationUtils.getMainAddress(location))
        chosenLocationDescription.postValue(LocationUtils.getLastAddress(location))
        fullLocation.postValue(location)
        getOriginLocation()
    }

    fun updateRoute(routes: List<LatLng>, int: Int) {
        route.postValue(routes)
        index.postValue(int)
    }

    fun getRoute(key: String, destination: String) =
        repository.getRoute(key, destination, preference.getAmbulanceOrigin().toString())
            .asLiveData()

    fun getBackRoute(key: String, origin: String) =
        repository.getRoute(key, preference.getAmbulanceOrigin().toString(), origin).asLiveData()
}