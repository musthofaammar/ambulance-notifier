package id.ateam.ambulancenotifier.ui.ambulance

import android.Manifest
import android.os.Bundle
import android.viewbinding.library.activity.viewBinding
import androidx.appcompat.app.AppCompatActivity
import androidx.work.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.databinding.ActivityAmbulanceLocationSenderBinding
import id.ateam.ambulancenotifier.utils.AmbulanceLocationSenderWorker
import id.ateam.ambulancenotifier.utils.SocketWorker
import io.socket.client.Socket
import kotlinx.coroutines.*
import org.koin.android.ext.android.inject
import pub.devrel.easypermissions.EasyPermissions
import java.util.concurrent.TimeUnit

class AmbulanceLocationSender : AppCompatActivity() {

    private lateinit var workManager: WorkManager
    private val mSocket: Socket by inject()
    private val binding: ActivityAmbulanceLocationSenderBinding by viewBinding()
    private var type = "listener"

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val dummyAmbulanceLocation = LatLng(-7.423431, 109.277053)

    private val permissions = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    companion object {

        const val FOREGROUND_lOCATION_REQUEST_CODE = 800
        const val BACKGROUND_lOCATION_REQUEST_CODE = 801

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ambulance_location_sender)

        workManager = WorkManager.getInstance(this)

        init()
        askPermission()
    }

    private fun askPermission() {
        if (EasyPermissions.hasPermissions(this, *permissions)) {

            if (EasyPermissions.hasPermissions(
                    this,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
                )
            ) {

                runAsListener()

            } else {

                runAsListener()

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                    EasyPermissions.requestPermissions(
                        this,
                        "Lokasi dibutuhkan untuk mengetahui jarak ambulan dengan anda",
                        FOREGROUND_lOCATION_REQUEST_CODE,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    )
                }
            }

        } else {
            EasyPermissions.requestPermissions(
                this,
                "Lokasi dibutuhkan untuk mengetahui jarak ambulan dengan anda",
                FOREGROUND_lOCATION_REQUEST_CODE,
                *permissions
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == FOREGROUND_lOCATION_REQUEST_CODE) {
            EasyPermissions.onRequestPermissionsResult(
                FOREGROUND_lOCATION_REQUEST_CODE,
                permissions,
                grantResults,
                this
            )
        } else {
            EasyPermissions.onRequestPermissionsResult(
                BACKGROUND_lOCATION_REQUEST_CODE,
                permissions,
                grantResults,
                this
            )
        }
    }

    private fun init() {

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        binding.btnChangeToListener.setOnClickListener {
            type = "listener"
            runAsListener()

        }

        binding.btnChangeToSender.setOnClickListener {
            type = "sender"
            runAsSender(dummyAmbulanceLocation)
        }
    }

    private fun runAsListener() {

        workManager.cancelAllWorkByTag("senderService")

        val listenerSocketWorker = PeriodicWorkRequestBuilder<SocketWorker>(
            3, TimeUnit.MINUTES
        )
            .setConstraints(
                Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
            )
            .setConstraints(Constraints.Builder().setRequiresBatteryNotLow(true).build())
            .addTag("listenerService")
            .build()

        workManager.enqueueUniquePeriodicWork(
            "Periodic Socket Service",
            ExistingPeriodicWorkPolicy.REPLACE,
            listenerSocketWorker
        )
    }

    private fun runAsSender(destination: LatLng? = null) {

        destination?.let {
            workManager.cancelAllWorkByTag("listenerService")

            mSocket.off("broadcast_ambulance_location")

            val senderWorker = OneTimeWorkRequestBuilder<AmbulanceLocationSenderWorker>()
                .setConstraints(
                    Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
                )
                .setConstraints(Constraints.Builder().setRequiresBatteryNotLow(true).build())
                .setInputData(
                    workDataOf(
                        "ambulance_id" to "AB191823",
                        "latitude" to destination.latitude,
                        "longitude" to destination.longitude
                    )
                )
                .addTag("senderService")
                .build()

            workManager.enqueueUniqueWork(
                "Periodic Socket Service",
                ExistingWorkPolicy.REPLACE,
                senderWorker
            )
        }
    }

    private fun observeWork(workRequest: OneTimeWorkRequest) {
        workManager
            .getWorkInfoByIdLiveData(workRequest.id)
            .observe(this, {
                if (it != null && it.state.isFinished) {
                    GlobalScope.launch {
                        withContext(Dispatchers.Main) {
                            delay(10000)
                            runAsSender()
                        }
                    }
                }
            })
    }
}