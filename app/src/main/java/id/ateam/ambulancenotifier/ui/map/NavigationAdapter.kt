package id.ateam.ambulancenotifier.ui.map

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.data.model.NavigationModel
import id.ateam.ambulancenotifier.databinding.ItemNavigationBinding

class NavigationAdapter: RecyclerView.Adapter<NavigationAdapter.ListViewHolder>() {
    private var listData = ArrayList<NavigationModel>()
    private var onItemClickCallback: OnItemClickCallback?= null

    interface OnItemClickCallback{
        fun onItemClicked(item: NavigationModel)
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }

    fun setData(data: ArrayList<NavigationModel>){
        listData.clear()
        listData = data
        notifyDataSetChanged()
    }

    inner class ListViewHolder(val binding: ItemNavigationBinding):  RecyclerView.ViewHolder(binding.root){
        fun bind(data: NavigationModel){
            with(binding){
                val listColor = listOf(Color.parseColor("#00BCD4"), Color.parseColor("#60FF4F"),Color.parseColor("#FFEB3B"))
                tvTitle.text = data.summary
                tvDistance.text = data.distance
                tvTime.text = data.time
                root.setBackgroundColor(listColor[data.index])
                root.setOnClickListener {
                    onItemClickCallback?.onItemClicked(data)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = ItemNavigationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        return holder.bind(listData[position])
    }

    override fun getItemCount(): Int {
        return listData.size
    }
}