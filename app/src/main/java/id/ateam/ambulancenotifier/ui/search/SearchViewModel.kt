package id.ateam.ambulancenotifier.ui.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import id.ateam.ambulancenotifier.data.Repository
import kotlinx.coroutines.FlowPreview

class SearchViewModel(private val repository: Repository): ViewModel() {
    @FlowPreview
    fun searchLocation(search: String, key: String)=
        repository.searchLocation(search, key).asLiveData()
}