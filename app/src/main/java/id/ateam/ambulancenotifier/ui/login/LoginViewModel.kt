package id.ateam.ambulancenotifier.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import id.ateam.ambulancenotifier.data.RemoteDataSource
import id.ateam.ambulancenotifier.data.Repository
import id.ateam.ambulancenotifier.data.networking.body.LoginBody
import kotlinx.coroutines.FlowPreview

class LoginViewModel(private val repository: Repository, private val remoteDataSource: RemoteDataSource): ViewModel() {
    fun login(body: LoginBody) = repository.login(body).asLiveData()
}