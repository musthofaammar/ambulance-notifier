package id.ateam.ambulancenotifier.ui.ambulance

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.viewbinding.library.activity.viewBinding
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.databinding.ActivityShowAlertBinding

class ShowAlertActivity : AppCompatActivity() {

    private val binding: ActivityShowAlertBinding by viewBinding()
    private lateinit var mediaPlayer: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_alert)

        showAlert()
        setButton()
    }

    private fun setButton() {
        binding.btnStop.setOnClickListener {
            mediaPlayer.stop()
        }
    }

    private fun showAlert() {
        try {
            mediaPlayer = MediaPlayer.create(this, R.raw.sirene_ambulance)
            mediaPlayer.start()
        } catch (e: Exception) {
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }
}