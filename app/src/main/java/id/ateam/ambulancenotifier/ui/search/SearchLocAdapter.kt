package id.ateam.ambulancenotifier.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.ateam.ambulancenotifier.data.model.LocationModel
import id.ateam.ambulancenotifier.databinding.ItemLocationBinding

class SearchLocAdapter: RecyclerView.Adapter<SearchLocAdapter.ListViewHolder>() {

    private val locations = ArrayList<LocationModel>()
    var onItemClick: ((LocationModel) -> Unit)? = null

    fun setData(locations: List<LocationModel>) {
        this.locations.clear()
        this.locations.addAll(locations)
        notifyDataSetChanged()
    }

    inner class ListViewHolder(private val binding: ItemLocationBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(location: LocationModel) {
            with(binding) {
                tvName.text = location.address
                tvDescription.text = location.details
            }
        }

        init {
            itemView.setOnClickListener { onItemClick?.invoke(locations[bindingAdapterPosition]) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val binding =
            ItemLocationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) =
        holder.bind(locations[position])

    override fun getItemCount() = locations.size
}