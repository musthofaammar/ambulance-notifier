package id.ateam.ambulancenotifier.ui.ambulance

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import androidx.work.WorkManager
import com.budiyev.android.codescanner.*
import id.ateam.ambulancenotifier.R
import id.ateam.ambulancenotifier.databinding.FragmentScanQRBinding
import id.ateam.ambulancenotifier.shared.BaseFragment
import io.socket.client.Socket
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class ScanQRFragment : BaseFragment() {
    private var _binding: FragmentScanQRBinding? = null
    override val binding: FragmentScanQRBinding
        get() = _binding!!
    override val viewModel: ViewModel
        get() = TODO("Not yet implemented")
    private val mViewModel by viewModel<AmbulanceViewModel>()
    private lateinit var codeScanner: CodeScanner

    private val mSocket: Socket by inject()
    private lateinit var workManager: WorkManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        workManager = WorkManager.getInstance(requireContext())
        _binding = FragmentScanQRBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            codeScanner = CodeScanner(requireActivity(), scannerView)

            codeScanner.apply {
                camera = CodeScanner.CAMERA_BACK
                formats = CodeScanner.ALL_FORMATS
                autoFocusMode = AutoFocusMode.SAFE
                scanMode = ScanMode.SINGLE
                isAutoFocusEnabled = true
                isFlashEnabled = false

                decodeCallback = DecodeCallback {
                    activity?.runOnUiThread {
                        mViewModel.getAmbulance(it.text.toInt()).observe(viewLifecycleOwner, {
                            if (it.type != null) {

                                workManager.cancelAllWorkByTag("listenerService")

                                mSocket.off("broadcast_ambulance_location")

                                findNavController().navigate(R.id.action_scanQRFragment_to_ambulanceDetailsFragment)
                            }
                        })
                    }
                }

                errorCallback = ErrorCallback {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireActivity(),
                            "Camera initialization error: ${it.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
            scannerView.setOnClickListener {
                codeScanner.startPreview()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    override fun observeLiveData() {
        null
    }
}