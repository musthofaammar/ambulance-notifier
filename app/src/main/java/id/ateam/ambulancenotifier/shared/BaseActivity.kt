package id.ateam.ambulancenotifier.shared

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding

abstract class BaseActivity : AppCompatActivity() {

    protected abstract val binding: ViewBinding
    protected abstract val viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initViews()
        observeLiveData()
    }

    open fun initViews() = Unit
    abstract fun observeLiveData()
}